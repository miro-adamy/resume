---
title: "Home"
date: 2018-02-10T18:56:13-05:00
sitemap:
  priority : 1.0

outputs:
- html
- rss
- json
---
<p>Senior DevOps / Cloud Engineer and Software Architect with experience leveraging agile, microservices, IaaC and CI/CD to build and manage large scale distributed platforms both in public and private cloud.</p>

<p>Multi-lingual, well travelled skilled professional - worked and lived in 7 countries on two continents.</p>
