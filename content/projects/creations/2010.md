{
    "title":"2010 - Thinkwrap Solutions, Ottawa, Canada",
    "link":"",
    "image":"",
    "description": "Team Lead / Architect - ATG",
    "featured":true,
    "tags":["Linux","Nagios","bash","AWS","Nginx", "Tomcat", "JBoss", "ATG", "Hybris"],
    "fact":"",
    "weight":"60",
    "sitemap": {"priority" : "0.8"}
}
 
#### Team Lead / Architect - Thinkwrap Solutions (evolution of Thinknostic), Ottawa, Canada

* Team Lead / Architect / Implementation (team 5 to 9)
* ATG2007.1, J2EE, JBoss, Tomcat, Apache HTTP, Linux
* store enhancements to support digital goods, integration with content providers
* ATG Commerce search reimplementation (replacing former system integrators)

