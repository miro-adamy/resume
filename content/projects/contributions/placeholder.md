{
    "title":"2018, 2019 Pivotree Valencia, Spain",
    "link":"",
    "image":"",
    "description":"After merge of Thinkwrap Commerce with Tenzing and Spark::red into Pivotree, fully focused on DevOps, cloud and Microservices",
    "featured":true,
    "tags":["Java","jQuery","REST APIs","Bamboo","JSON"],
    "fact":"",
    "weight":"100",
    "sitemap": {"priority" : "0.8"}
}

* DevOps/Cloud Engineer
* AWS environments management and provisioning
* Migration of 2 projects from DockerCloud to Kubernetes
* Internal training for Docker and Kubernetes - certification program for AWS
* AWS, GCP, Kubernetes, Docker, MongoDB
* BigData/Machine learning platform infrastructure support
* Terraform + Terraform Enterprise
* Python 3 + libraries (Spark/PySpark, NumPy, Pandas)
